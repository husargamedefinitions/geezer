﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour {

    public int Coins = 0;
    void OnTriggerEnter2D (Collider2D other)
    {
        if( other.tag == "Coin" )
        {
            Coins++;
            Destroy( other.gameObject );
            Debug.Log( Coins );
        }
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
